<?php

include_once 'app.php';

$userService = new \Services\UserService($db);
$response = array("error" => FALSE);

if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['confirm']) && isset($_POST['role_id']))
{
    $username = $_POST['username'];
    $password = $_POST['password'];
    $confirm = $_POST['confirm'];
    $role_id = $_POST['role_id'];

    if ($userService->exists($username)) {
        $response['error'] = TRUE;
        $response['error_msg'] = "Потребителското име вече съществува!";
        echo json_encode($response);
    } elseif (!$userService->isPasswordMatch($password, $confirm)) {
        $response['error'] = TRUE;
        $response['error_msg'] = "Паролите не съвпадат!";
        echo json_encode($response);
    } else {
        $user = $userService->register($username, $password, $role_id);
        if ($user) {
            // user stored successfully
            $response["error"] = FALSE;
            /*$response["uid"] = $user["unique_id"];
            $response["user"]["username"] = $user["username"];
            */
            $response['msg'] = "Успешна регистрация!";
            echo json_encode($response);
        } else {
            // user failed to store
            $response["error"] = TRUE;
            $response["error_msg"] = "Неизвестна грешка при регистрация!";
            echo json_encode($response);
        }
    }
}  else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Задължителни параметри (username, password, confirm, role) липсват!";
    echo json_encode($response);
}