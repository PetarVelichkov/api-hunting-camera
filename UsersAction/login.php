<?php

include_once "app.php";

$response = array("error" => FALSE);
$userService = new \Services\UserService($db);

if (isset($_POST['username']) && isset($_POST['password']))
{
    // receiving the post params
    $username = $_POST['username'];
    $password = $_POST['password'];

    // get the user by username and password
    $user = $userService->login($username, $password);
    if ($user) {
        // user is found
        $response["error"] = FALSE;
        $response["user"]["uid"] = $user["unique_id"];
        $response["user"]["username"] = $user["username"];
        $response["user"]["role_id"] = $user["role_id"];
        echo json_encode($response);
    } else {
        // user is not found with the credentials
        $response["error"] = TRUE;
        $response["error_msg"] = "Грешно име или парола!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "Името и паролата са задължителни!";
    echo json_encode($response);
}