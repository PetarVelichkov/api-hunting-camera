<?php
include_once "app.php";

$response = array("error" => FALSE);
$userService = new \Services\UserService($db);

if (isset($_POST['userId']) && $_POST['password']) {
    $id = $_POST['userId'];
    $password = $_POST['password'];
    $res = $userService->ChangePassword($id, $password);
    if ($res)
    {
        $response["error"] = FALSE;
        $response['msg'] = "Успешна промяна на парола!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Не е избрана парола!";
    echo json_encode($response);
}
//exit;
