<?php
include_once "app.php";

$response = array("error" => FALSE);
$userService = new \Services\UserService($db);

if (isset($_POST['userId'])) {
    $userId = $_POST['userId'];
    $res = $userService->DeleteUser($userId);
    if ($res)
    {
        $response["error"] = FALSE;
        $response['msg'] = "Успешно изтриване!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Не е избран потребител!";
    echo json_encode($response);
}

//exit;