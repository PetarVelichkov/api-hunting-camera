<?php
include_once "app.php";
include_once "Config/Config.php";

$userService = new \Services\UserService($db);
$imageService = new \Services\ImageService($db);
$_POST = json_decode(file_get_contents("php://input"),true);
$apiCall = $_POST["apiCall"];
$from_date = $_POST["from"];
$to_date = $_POST["to"];

if ($apiCall === "login") {
    include_once 'UsersAction/login.php';
} elseif ($apiCall === 'register') {
    include_once 'UsersAction/register.php';
} elseif ($apiCall === 'getAllUsers') {
    echo json_encode($userService->getAllUsers());
} elseif ($apiCall === "deleteUser") {
    include_once 'UsersAction/deleteUser.php';
} elseif ($apiCall === "changePassword") {
    include_once "UsersAction/changeUserPassword.php";
} elseif ($apiCall == "getImages") {
    echo json_encode($imageService->getAllImages($from_date, $to_date));
} elseif ($apiCall === 'deleteImages') {
    $images = $_POST['images'];
    $imageService->DeleteImages($images);
    echo json_encode($imageService->getAllImages($from_date, $to_date));
} elseif ($apiCall === 'insert') {
    $imageService->insertImages($imageService->getImagesForInsertDB());
}
