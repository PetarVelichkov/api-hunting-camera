<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
date_default_timezone_set("Europe/Sofia");

include_once "Adapter/PDODatabase.php";
include_once "Adapter/PDODatabaseStatement.php";
include_once "Config/Config.php";
include_once "Models/User.php";
include_once "Services/UserService.php";
include_once "Services/ImageService.php";


$db = new \Adapter\PDODatabase(
    DB_HOST,
    DB_NAME,
    DB_USER,
    DB_PASSWORD
);