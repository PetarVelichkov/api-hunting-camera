<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 13.12.2018 г.
 * Time: 15:08
 */

namespace Services;


interface ImagesServiceInterface
{
    public function getAllImages($from_date, $to_date);

    public function deleteImages($images);
}