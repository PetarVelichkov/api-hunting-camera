<?php

namespace Services;

include_once "Config/Config.php";
include "ImagesServiceInterface.php";
use Adapter\DatabaseInterface;

class ImageService implements ImagesServiceInterface
{
    private $db;

    function __construct(DatabaseInterface $db)
    {
        $this->db = $db;
    }

    public function getAllImages($from_date, $to_date)
    {
        $stmt = $this->db->prepare("
         SELECT id, image 
         FROM images
         WHERE imgDate BETWEEN '$from_date' AND '$to_date'
         ORDER BY id DESC 
        ");
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);

    }

    public function DeleteImages($images)
    {
        foreach ($images as $image) {
            $replaced = str_replace("https:/", "..", $image);
            $id = $image["id"];
            $stmt = $this->db->prepare("
                DELETE FROM images
                WHERE id = :id
            ");
            if (is_file($replaced['image'])) {
                unlink($replaced['image']);
            }
            $stmt->execute(
                [
                    "id" => $id
                ]
            );
        }
    }

    public function getImagesForInsertDB()
    {
        //TODO ../../
        $images = glob(IMG_HOST . '*.{jpg}', GLOB_BRACE);
        //$images = glob("gallery-images/" . '*.{jpg}', GLOB_BRACE);

        return $images;
    }

    public function insertImages($images)
    {
        $ImagesForInsert = [];
        $imageForCompare = [];
        $stmt = $this->db->prepare("
            SELECT image FROM images
        ");
        $stmt->execute();
        $dataInDb = $stmt->fetchAll(\PDO::FETCH_COLUMN);
        if ((count($dataInDb) === count($images))) {
            return false;
        }
        foreach ($images as $k => $v) {
            $split = explode("/", $v);
            array_push($imageForCompare, API_IMG_HOST . $split[2] . '/' . $split[3]);
        }
        $diff = array_diff($imageForCompare, $dataInDb);

        foreach ($diff as $key => $value) {
            $val = explode("_", $value);
            $year = substr($val[2], 0, 4);
            $month = substr($val[2], 4, 2);
            $day = substr($val[2], 6, 2);

            $dateStr = $year . '-' . $month . '-' . $day;
            $date = date("Y-m-d", strtotime($dateStr));

            $ImagesForInsert[$key] = [
                "image" => $value,
                "imgDate" => $date
            ];
        }

        foreach ($ImagesForInsert as $key => $value) {
            $image = $value['image'];
            $imgDate = $value['imgDate'];

            $stmt = $this->db->prepare("
                INSERT IGNORE INTO images (`image`, `imgDate`)
                VALUES (:image, :imgDate)
        ");
            $stmt->execute(
                [
                    "image" => $image,
                    "imgDate" => $imgDate
                ]
            );
        }

    }
}