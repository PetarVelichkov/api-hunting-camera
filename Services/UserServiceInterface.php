<?php

namespace Services;


interface UserServiceInterface
{
    public function register($username, $password, $role);

    public function DeleteUser($id);

    public function ChangePassword($id, $password);

    public function exists($username): bool;

    public function isPasswordMatch($password, $confirm): bool;

    public function login($username, $password);
}