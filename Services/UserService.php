<?php

namespace Services;

include "UserServiceInterface.php";
use Adapter\DatabaseInterface;

class UserService implements UserServiceInterface
{
    private $db;

    function __construct(DatabaseInterface $db)
    {
        $this->db = $db;
    }

    public function register($username, $password, $role_id)
    {
        $unique_id = uniqid('', true);
        $hash = $this->hashSSHA($password);
        $password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt

        $stmt = $this->db->prepare("
                INSERT INTO users 
                SET 
                    unique_id = :unique_id,
                    username = :username,
                    password = :password,
                    salt = :salt,
                    role_id = :role_id
       ");

        return $stmt->execute(
            [
                "unique_id" => $unique_id,
                "username" => $username,
                "password" => $password,
                "salt" => $salt,
                "role_id" => $role_id
            ]
        );
    }

    public function DeleteUser($id)
    {
        $stmt = $this->db->prepare("
                DELETE FROM users
                WHERE id = :id
        ");

        return $stmt->execute(
            [
                "id" => $id
            ]
        );
    }

    public function getAllUsers()
    {
        $stmt = $this->db->prepare("
        SELECT id, unique_id, username FROM users
        ");

        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function ChangePassword($id, $password)
    {
        $hash = $this->hashSSHA($password);
        $password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt

        $stmt = $this->db->prepare("
                UPDATE users
                SET 
                  password = :password,
                  salt = :salt
                WHERE id = :id
        ");

        return $stmt->execute(
            [
                "password" => $password,
                "salt" => $salt,
                "id" => $id
            ]
        );
    }

    public function exists($username): bool
    {
        $stmt = $this->db->prepare("
                SELECT * FROM users
                WHERE username = ?
        ");

        $stmt->execute(
            [
                $username
            ]
        );

        return !!$stmt->fetchRow();
    }

    public function isPasswordMatch($password, $confirm): bool
    {
        return $password === $confirm;
    }

    public function login($username, $password)
    {
        $stmt = $this->db->prepare("
                SELECT * FROM users WHERE username = ?
        ");
        $stmt->execute(
            [
                $username
            ]
        );
        $user = $stmt->fetchRow();
        if ($user) {
            // verifying user password
            $salt = $user['salt'];
            $encrypt_password = $user['password'];
            $hash = $this->checkhashSSHA($salt, $password);
            // check for password equality
            if ($encrypt_password === $hash) {
                // user authentication details are correct
                return $user;
            }
            //return NULL;
        } else {
            return NULL;
        }
    }

    /**
     * Encrypting password
     * @param password
     * returns salt and encrypted password
     * @return array
     */
    public function hashSSHA($password) {

        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }

    /**
     * Decrypting password
     * @param salt , password
     * returns hash string
     * @return string
     */
    public function checkhashSSHA($salt, $password) {

        $hash = base64_encode(sha1($password . $salt, true) . $salt);

        return $hash;
    }
}